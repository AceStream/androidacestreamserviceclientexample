Android AceStream Service client example
----------------------------------------

This sample application demonstrates, how client can bind to AceStreamEngineService. There are two ways to bind to service:

  - **Using AIDL**:
  To bind to service you need to use intent with action named the same as IAceStreamEngine.class (org.acestream.engine.service.v0.IAceStreamEngine.class.getName()) and to receive callback from service you need IAceStreamEngineCallback implementation. Example of realisation you can find in AIDLClient.

  - **Using Messenger**:
  To bind to service you need to use explicit name of AceStreamEngineService (org.acestream.engine.service.AceStreamEngineService) and use Messenger to communicate with it. MessengerClient demonstrates the simple way for using this method.

When binding to the service you should explicitly set package name in the intent.
Ace Stream is distributed with several different applications IDs, so the package name is not fixed.
To find the right package name you should find all installed Ace Stream applications and then select the one with the highest version code among them.
Installed Ace Stream apps can be found with well-known applications IDs and with implicit service intent.
See the code and comments of method ``BaseClient.getServicePackage`` to find out how to do this.

This example automatically selects one of these package names after checking which app is installed (see BaseClient.getServicePackage() method).

Latest AIDL interfaces you can find in the code.

Note: The application should be built in Android Studio.

Note: Using AceStreamEngineService via Messenger was broken until version 3.1.25 (version code 30125000), so only AIDL is supported before this version.