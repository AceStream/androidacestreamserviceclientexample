package org.acestream.engine.service.example.client;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseClient {
	private final static String TAG = "BaseClient";

	private Context mContext;
	private Handler mHandler;
	
	BaseClient(Context context) {
		mContext = context;
	}
	
	public void setHandler(Handler handler) {
		mHandler = handler;
	}
	
	final Context getContext() {
		return mContext;
	}
	
	final Handler getHandler() {
		return mHandler;
	}

	/**
	 * Select which Ace Stream application to connect to.
	 * If no Ace Stream apps are installed return null.
	 * If several Ace Stream apps are installed then select one with highest version code.
	 *
	 * @param ctx Any valid Context (used to resolve intents)
	 * @return Selected application ID or null.
	 */
	static String getServicePackage(Context ctx) {
		String selectedPackage = null;
		int maxVersion = -1;

		// Place all found Ace Streams apps here with version codes
		List<Pair<String,Integer>> packages = new ArrayList<>();

		// First, find well-known apps
		List<String> knownPackages = new ArrayList<String>(){
			{
				add("org.acestream.media");
				add("org.acestream.media.atv");
				add("org.acestream.core");
				add("org.acestream.core.atv");
			}
		};
		for(String packageName: knownPackages) {
			int version = getAppVersion(ctx, packageName);
			if(version != -1) {
				Log.v(TAG, "getServicePackage: found known: id=" + packageName + " version=" + version);
				packages.add(new Pair<>(packageName, version));
				if(version > maxVersion) {
					maxVersion = version;
				}
			}
		}

		// Second, find apps by implicit service intent.
		// Only apps starting from version 3.1.30.1 (code 301301000) can be found with intent.
		Intent intent = new Intent("org.acestream.engine.service.v0.IAceStreamEngine");
		List<ResolveInfo> services = resolveIntent(ctx, intent);
		if(services != null) {
			for (ResolveInfo ri : services) {
				int version = getAppVersion(ctx, ri.serviceInfo.packageName);
				if(version != -1) {
					if(!knownPackages.contains(ri.serviceInfo.packageName)) {
						Log.v(TAG, "getServicePackage: found by service: id=" + ri.serviceInfo.packageName + " version=" + version);
						packages.add(new Pair<>(ri.serviceInfo.packageName, version));
						if (version > maxVersion) {
							maxVersion = version;
						}
					}
				}
			}
		}

		// Select package with the max version
		for(Pair<String,Integer> item: packages) {
			if(item.second == maxVersion) {
				selectedPackage = item.first;
				break;
			}
		}

		Log.v(TAG, "getServicePackage: selected: id=" + selectedPackage);

		return selectedPackage;
	}

	static int getAppVersion(Context ctx, String packageName) {
		int versionCode = -1;
		try {
			PackageInfo pkgInfo = ctx.getPackageManager().getPackageInfo(packageName, 0);
			versionCode = pkgInfo.versionCode;
		} catch (PackageManager.NameNotFoundException e) {
			Log.v(TAG, "Failed to get package version: " + e.getMessage());
		}
		return versionCode;
	}

	private static List<ResolveInfo> resolveIntent(Context ctx, Intent intent) {
		PackageManager pm = ctx.getPackageManager();
		List<ResolveInfo> resolveInfo = pm.queryIntentServices(intent, PackageManager.MATCH_DEFAULT_ONLY);
		if(resolveInfo == null || resolveInfo.size() == 0) {
			Log.d(TAG, "resolveIntent: nothing found");
			return null;
		}

		return resolveInfo;
	}
	
	public abstract void bind();
	
	public abstract void unbind();

}
