package org.acestream.engine.service.example.client;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AIDLFragment extends BaseFragment {

	@Override
	public View onCreateView(@NonNull  LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_aidl, container, false);

		initServiceClient(new AIDLClient(inflater.getContext().getApplicationContext()));
		initView(view);

		setRetainInstance(true);
		return view;
	}
	
}
