package org.acestream.engine.service.example.client;

import org.acestream.engine.service.v0.AceStreamEngineMessages;
import org.acestream.engine.service.v0.IAceStreamEngine;
import org.acestream.engine.service.v0.IAceStreamEngineCallback;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.widget.Toast;

public class AIDLClient extends BaseClient {
	private final static String TAG = "AIDLClient";

	private IAceStreamEngine mService = null;
	private boolean mBound = false;
	
	private IAceStreamEngineCallback mCallback = new IAceStreamEngineCallback.Stub() {
		@Override
		public void onUnpacking() {
			Handler handler = getHandler();
			Message message = handler.obtainMessage(AceStreamEngineMessages.MSG_ENGINE_UNPACKING);
			handler.sendMessage(message);
		}
		@Override
		public void onStarting() {
			Handler handler = getHandler();
			Message message = handler.obtainMessage(AceStreamEngineMessages.MSG_ENGINE_STARTING);
			handler.sendMessage(message);
		}
		@Override
		public void onReady(int listenPort) {
			Handler handler = getHandler();
			int httpApiPort = 0;
			if(mService != null) {
				try {
					httpApiPort = mService.getHttpApiPort();
				}
				catch(RemoteException e) {
					Log.e(TAG, "Failed to get HTTP API port", e);
				}
			}
			Message message = handler.obtainMessage(AceStreamEngineMessages.MSG_ENGINE_READY, listenPort, httpApiPort);
			handler.sendMessage(message);
		}
		@Override
		public void onWaitForNetworkConnection() {
			Handler handler = getHandler();
			Message message = handler.obtainMessage(AceStreamEngineMessages.MSG_ENGINE_WAIT_CONNECTION);
			handler.sendMessage(message);
		}

		@Override
		public void onPlaylistUpdated() {
		}

		@Override
		public void onEPGUpdated() {
		}

		@Override
		public void onRestartPlayer() {
		}

		@Override
		public void onStopped() {
			Handler handler = getHandler();
			Message message = handler.obtainMessage(AceStreamEngineMessages.MSG_ENGINE_STOPPED);
			handler.sendMessage(message);
		}
	};
	
	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "onServiceDisconnected: package=" + name.getPackageName());
			mService = null;
			mBound = false;
		}
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG, "onServiceConnected: package=" + name.getPackageName());
			mService = IAceStreamEngine.Stub.asInterface(service);
			try {
				mService.registerCallback(mCallback);
				mService.startEngine();
			} catch (RemoteException e) {
				Log.e(TAG, "error", e);
			}
		}
	};
	
	AIDLClient(Context context) {
		super(context);
	}

	@Override
	public void bind() {
		if(mBound) {
			Toast.makeText(getContext(), "Already bound", Toast.LENGTH_SHORT).show();
		}
		else {
			String servicePackage = getServicePackage(getContext());
			if(servicePackage == null) {
				Toast.makeText(getContext(), "Ace Stream is not installed", Toast.LENGTH_SHORT).show();
				return;
			}
			Intent intent = new Intent(org.acestream.engine.service.v0.IAceStreamEngine.class.getName());
			intent.setPackage(servicePackage);
			mBound = getContext().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
			if(!mBound) {
				Toast.makeText(getContext(), "Failed to bind", Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void unbind() {
		if(mBound) {
			if(mService != null) {
				try {
					mService.unregisterCallback(mCallback);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
			getContext().unbindService(mConnection);
			mBound = false;
			Toast.makeText(getContext(), "Unbind done", Toast.LENGTH_SHORT).show();
		}
		else {
			Toast.makeText(getContext(), "Not bound", Toast.LENGTH_SHORT).show();
		}
	}
}
