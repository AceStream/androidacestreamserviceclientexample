package org.acestream.engine.service.example.client;

import org.acestream.engine.service.v0.AceStreamEngineMessages;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.widget.Toast;

public class MessengerClient extends BaseClient {
	private final static String TAG = "MessengerClient";

	private Messenger mService = null;
	private boolean mBound = false;
	
	private Messenger mMessenger = null;
	
	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d(TAG, "onServiceDisconnected: package=" + name.getPackageName());
			mService = null;
			mBound = false;
		}
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			Log.d(TAG, "onServiceConnected: package=" + name.getPackageName());
			mMessenger = new Messenger(getHandler());
			mService = new Messenger(service);
			try {
				Message msg = Message.obtain(null, AceStreamEngineMessages.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);

				msg = Message.obtain(null, AceStreamEngineMessages.MSG_START);
				mService.send(msg);
			} catch (Throwable e) {
				Log.e(TAG, "error", e);
			}
		}
	};
	
	MessengerClient(Context context) {
		super(context);
	}

	@Override
	public void bind() {
		if(mBound) {
			Toast.makeText(getContext(), "Already bound", Toast.LENGTH_SHORT).show();
		}
		else {
			String servicePackage = getServicePackage(getContext());
			if(servicePackage == null) {
				Toast.makeText(getContext(), "Ace Stream is not installed", Toast.LENGTH_SHORT).show();
				return;
			}

			// Messenger was broken until version 3.1.25 (30125000)
			int versionCode = getAppVersion(getContext(), servicePackage);
			if(versionCode < 30125000) {
				Toast.makeText(getContext(), "Cannot use Messenger with Ace Stream before version 3.1.25", Toast.LENGTH_SHORT).show();
				return;
			}

			Intent intent = new Intent();
			intent.setClassName(servicePackage, "org.acestream.engine.service.AceStreamEngineService");
			mBound = getContext().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
			if(!mBound) {
				Toast.makeText(getContext(), "Failed to bind", Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void unbind() {
		if(mBound) {
			if(mService != null) {
				Message msg = Message.obtain(null, AceStreamEngineMessages.MSG_UNREGISTER_CLIENT);
				msg.replyTo = mMessenger;
				try {
					mService.send(msg);
				} catch (Throwable e) {
					Log.e(TAG, "unbind: error", e);
				}
			}
			getContext().unbindService(mConnection);
			mBound = false;
			Toast.makeText(getContext(), "Unbind done", Toast.LENGTH_SHORT).show();
		}
		else {
			Toast.makeText(getContext(), "Not bound", Toast.LENGTH_SHORT).show();
		}
	}
}
